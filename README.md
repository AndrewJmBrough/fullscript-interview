#Intro
This project was created to show off some react skills for a Fullscript Interview.

It uses react, normalize.css for consistent rendering across browsers and axios to simplify https requests to the rickandmortyapi.

There are a few tests, the main example is a unit test checking if a function returns 6 random characters and that there are no duplicates. It uses test data saved from the rickandmortyapi.

#Description
The app is a guessing game that asks you to select all characters from a list who appear in an episode.

Each question is formed by picking an episode at random and displaying 3 characters (random between known number of characters) below it. User must click on the characters and hit okay. Front end logic will see if their selection matches and show results.

#Setup
This is a node project and assumes you have/know how to use npm to run it.
1. npm install
2. npm run start
3. ???
4. Profit (play the game, but I only tested this on one machine so fingers crossed)