import React, { Component } from 'react';
import 'normalize.css/normalize.css'
import './App.scss'
import Header from './Header';
import GuessingGame from './GuessingGame/GuessingGame';
import TitleScreen from './TitleScreen/TitleScreen';

class App extends Component {

  constructor(props) {
    
    super(props);
    this.state = {
    	gameStarted: true
    };
  }
  
  render() {

    var page = null;
    if (this.state.gameStarted) {
      page = <GuessingGame />
    } else {
      page = <TitleScreen startGame={this.startGame} />
    }
    return(
      <div className="App">
        <Header />
        {page}
      </div>
    )
  }

  startGame = (isStarted) => {

    this.setState({
      gameStarted: isStarted
    })
  }
}

export default App;
