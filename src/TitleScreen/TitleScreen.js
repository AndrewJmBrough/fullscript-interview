import React, { Component } from 'react';

export default class TitleScreen extends Component {

    render() {
      return(
        <div className="title-screen">
          <h1>Rick and Morty's</h1>
          <h2>Totally Rickdickulous Guessing Game</h2>
          <button className="start-button"
            onClick={(e) => this.props.startGame(true)}>Start the game already <div>Damnit Morty, always waiting around for you come on lets start this M!&%(#$&^@)$&</div></button>
        </div>
      )
    }
}