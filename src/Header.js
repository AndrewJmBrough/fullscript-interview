import React, { Component } from 'react';
import './Header.scss';

export default class Header extends Component {

    render() {

        return(
            <header>
                <img src="/img/rick-and-morty-logo.png" alt="Rick and Morty logo"/>
                <h1>The Essentially Unessential Guessing Game</h1>
            </header>
        )
    }
}