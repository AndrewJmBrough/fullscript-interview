import axios from 'axios'
import GuessingGame from './GuessingGame';
import testEpisode from './test/episode.json';
import testCharacter from './test/character.json';


//NOTE api tests aren't working due to inability to make HTTPS requests from local server.
//Leaving anyways because they're interesting.

//test that the api is working
//also that the episode and endpoint locations are the same
it('API is online', () => {
    
    axios.get('https://rickandmortyapi.com/api').then(response=>{
        console.log("api response");
    }).catch(error=>{
        // console.error(error);
    });
});

//test that the episode and character structure is as expected (so I'm not null checking everything)
it('API Episode format', () => {

    axios.get('https://rickandmortyapi.com/api/episode/1').then(response => {
        var episode = response.data;
        if(episode.characters) {
            console.log("API Episode format is OK")
        } else {
            console.error("API Episode didn't have correct format: missing characters", response);
        }
    }).catch(error => {
        // console.error(error);
    });
});

it('API Character format', () => {

    axios.get('https://rickandmortyapi.com/api/character/1').then(response => {
        var character = response.data;
        if (character.name) {
            console.log("API Character format is OK")
        } else {
            console.error("API Character missing name property", response);
        }
    }).catch(error => {
        // console.error(error);
    });
});

//THIS is a passable test, forget the above
//unit test for getting a list of 6 character, some from the episode and some random from all characters.
it('get random characters', () => {

    // console.log(testEpisode, testCharacter);
    var characters = GuessingGame.getRandomCharacters(testEpisode.results[0], testCharacter.info, 6);
    if(characters.length != 6) console.error("characters isn't 6 long");
    characters.forEach(char=>{
        var matches = characters.filter(char2=>{
            return char == char2;
        })
        if(matches.length > 1) console.error(char + " occurs more than once");
    })
});