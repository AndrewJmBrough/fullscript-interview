import React, { Component } from 'react';
import './Card.scss';

export default class Card extends Component {

    render() {

        var classes = ["card"]
        if(this.props.character.selected) classes.push("selected");
        if (this.props.character.isInEpisode) classes.push("isInEpisode");
        return(
            <a className={classes.join(" ")} style={{
                backgroundImage: 'url('+this.props.character.image+')'
            }}
                onClick={(e)=>this.props.selectCard(this.props.character)}>
                <h3>{this.props.character.name}</h3>
            </a>
        )
    }
}