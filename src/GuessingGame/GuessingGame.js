import React, { Component } from 'react'
import './GuessingGame.scss'
import axios from 'axios'
import Card from './Card'

var charactersToShow = 6;
export default class GuessingGame extends Component {

  constructor(props) {

    super(props);
    this.state = {
      loading: true,
      nextPage: 1,
      targetCharacters: [],
      selectedCharacters: [],
      showMatches: false
    };

  }

  componentDidMount() {

    this.getEpisodes();
  }

  /////////////////////////////
  /////////Data Logic//////////
  /////////////////////////////

  //gets a list of locations from a random page
  getEpisodes() {

    this.setState({
      loading: true
    }, ()=>{

      axios.get('https://rickandmortyapi.com/api/episode', {
        page: this.state.nextPage
      }).then(this.getEpisodesSuccess)
        .catch(this.getEpisodesFail);
    })
  }

  getEpisodesSuccess = (response) => {

    try {
      console.log(response);

      var nextPage = parseInt(Math.random() * response.data.info.pages);

      var episodes = response.data.results;
      var targetEpisode = episodes[parseInt(Math.random() * episodes.length)];

      this.setState({
        episodesInfo: response.data.info,
        episodes: episodes,
        nextPage: nextPage,
        targetEpisode: targetEpisode
      }, ()=>{
        this.getCharacters();
      })
    } catch (e) {
      console.error(e);
      this.setState({
        error: e
      })
    }
  }

  getEpisodesFail = (response) => {

    console.error(response);
    this.setState({
      error: "Failed to get episodes. The API or your internet must be down.... I don't know why you're surprised, this is a live demo after all and that always happens."
    })
  }

  getCharacters() {

    if(!this.state.charactersInfo) {
      //go get info about characters so we can pick random ones from known list
      //this is mainly only used for knowing the total number of characters
      //so we can pick a random one successfully
      axios.get('https://rickandmortyapi.com/api/character')
        .then(this.getCharactersSuccess)
        .catch(this.getCharactersFail);
    } else {
      //just use the exising knowledge of characters
      this.getTargetCharacters(this.state.targetEpisode, this.state.charactersInfo);
    }
  }

  getCharactersSuccess = (response) => {

    try {

      this.setState({
        charactersInfo: response.data.info
      }, ()=>{
        this.getTargetCharacters(this.state.targetEpisode, this.state.charactersInfo)
      })
    } catch(e) {
      console.error(e);
      this.setState({
        error: e
      })
    }
  }

  getCharactersFail = (response) => {

    console.error(response);
    this.setState({
      error: "Failed to get Characters. The API or your internet must be down.... I don't know why you're surprised, this is a live demo after all and that always happens."
    })
  }

  //get a list of characters to choose from who may or may not appear in the episode
  //Pick a random number between 1 and 6 characters who are definitely in the episode, so there IS a correct answer
  //fill list with random characters otherwise that may or may not be in the episode
  //no duplicate characters.
  getTargetCharacters(episode, charactersInfo) {

    var targetCharacterIndices = GuessingGame.getRandomCharacters(episode, charactersInfo)

    targetCharacterIndices.forEach(id =>{
      axios.get('https://rickandmortyapi.com/api/character/' + id)
        .then(this.getSingleCharacterSuccess)
        .catch(this.getSingleCharacterFail);
    })
  }

  //return an array of character indexes, some in the episode and some not, at random
  static getRandomCharacters(episode, charactersInfo, charactersToGet = charactersToShow) {

    var numWillMatch = parseInt(Math.random() * 5 + 1); //will always have at least one and up to 6 matches
    charactersToGet -= numWillMatch;
    var targetCharacterIndices = [];
    for (var i = 0; i < numWillMatch; i++) {
      var tCharIndex = null;
      while (tCharIndex === null) {
        var randomCharIndex = GuessingGame.getRandomCharacterIdFromEpisode(episode);
        if (targetCharacterIndices.indexOf(randomCharIndex) === -1) tCharIndex = randomCharIndex; //break out when found a new char that isn't already in targetCharacters
      }
      targetCharacterIndices.push(tCharIndex);
    }
    for (var i = 0; i < charactersToGet; i++) {
      var tCharIndex = null;
      while (tCharIndex === null) {
        var randomCharIndex = parseInt(Math.random() * charactersInfo.count);
        if (targetCharacterIndices.indexOf(randomCharIndex) === -1) tCharIndex = randomCharIndex; //break out when found a new char that isn't already in targetCharacters
      }
      targetCharacterIndices.push(tCharIndex);
    }

    return targetCharacterIndices;
  }

  static getRandomCharacterIdFromEpisode(episode) {

    var charUrl = episode.characters[parseInt(Math.random() * episode.characters.length)];
    return GuessingGame.getCharIndexFromUrl(charUrl)
  }

  getSingleCharacterSuccess = (response) => {

    var targetCharacters = this.state.targetCharacters;
    console.log("Loaded character: ", response);
    targetCharacters.push(response.data)
    this.setState({
      targetCharacters: targetCharacters,
      loading: targetCharacters.length < charactersToShow
    })
  }

  getSingleCharacterFail = (response) => {

    console.error(response);
    this.setState({
      error: "Failed to get info for a character! You dun goofed bad Morty. Or, OR MORTY OR The API or your internet might be down."
    })
  }

  /////////////////////////////
  ////////Render Blocks////////
  /////////////////////////////

  render() {

    console.log(this.state);

    if(this.state.loading) {
      return(
        <p>Loading... <small>wow Morty why is this taking long enough for you to actually read this Morty this is unbelievable Morty what did you break Morty WHAT DID YOU BREAK WE'RE ALL GONNA DIE</small></p>
      )
    }

    var gameClasses = ["guessing-game"]
    if(this.state.showMatches) gameClasses.push("show-matches");
    return(
      <div className={gameClasses.join(" ")}>
        <h2>Who was in the episode <span className="episode-title">{this.state.targetEpisode.name}?</span></h2>        
        {this.renderAnswerBlock()}
        {!this.state.showMatches ? 
        <button className="submit-answer"
          onClick={this.onSubmitGuesses}>That's it, I've made my decision</button> :
        <button className="next-question"
          onClick={this.onNextQuestion}>Okay fine next question already</button>

          }
      </div>
    )
  }

  renderAnswerBlock() {

    var cards = this.state.targetCharacters.map((character, index)=>{
      return (
        <Card
          key={index} //required by react
          character={character}
          selectCard={this.selectCard}/>
      )
    })

    return(
      <div className="answer-block">
        {cards}
      </div>
    )
  }

  /////////////////////////////
  ////Interaction & Gameplay///
  /////////////////////////////

  //select or deselect card - flags character using 'selected' boolean
  //triggers re-render of cards so selected are highlighted 
  selectCard = (character) => {

    var targetCharacters = this.state.targetCharacters;
    var tChar = targetCharacters.find(char =>{
      return char === character
    });
    tChar.selected = !tChar.selected;
    
    this.setState({
      targetCharacters: targetCharacters
    })
  }

  onSubmitGuesses = (e) => {

    this.checkCorrectAnswers(this.state.targetCharacters, this.state.targetEpisode);
  }

  checkCorrectAnswers(targetCharacters, episode) {

    targetCharacters = this.markCharactersInEpisode(targetCharacters, episode);
    var numMatches = this.countCorrectAnswers(targetCharacters);
    
    this.setState({
      showMatches: true,
      roundScore: numMatches,
      targetCharacters: targetCharacters //update with match flag
    })
  }

  markCharactersInEpisode(targetCharacters, episode) {

    targetCharacters.forEach(char => {
      var matchingChar = episode.characters.find(episodeCharUrl => {
        var episodeCharIndex = GuessingGame.getCharIndexFromUrl(episodeCharUrl);
        if (episodeCharIndex == char.id) {
          return episodeCharUrl
        }
      })
      if (matchingChar) {
        char.isInEpisode = true;
      }
    })
    return targetCharacters;
  }

  countCorrectAnswers(targetCharacters) {

    var numMatches = 0;
    targetCharacters.forEach(tChar => {
      if (tChar.isInEpisode && tChar.selected) numMatches++;
    })

    return numMatches;
  }

  static getCharIndexFromUrl(charUrl) {

    var slicedUrl = charUrl.slice(charUrl.lastIndexOf('/'), charUrl.length)
    return slicedUrl[1]; //this will be the index of the char, which mirrors the index in the list of ALL characters, as returned by the API
  }

  onNextQuestion = (e) => {

    //restarts flow, gets new questions
    this.setState({
      targetEpisode: null,
      targetCharacters: [],
      showMatches: false,
      loading: true
    }, ()=>{
      this.getEpisodes();
    })
    
  }
}